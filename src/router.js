import Vue from "vue";
import Router from "vue-router";
import PlaceOrders from "./views/PlaceOrders.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "PlaceOrders",
      component: PlaceOrders
    },
    {
      path: "/view-orders",
      name: "ViewOrders",
      component: () =>
        import(/* webpackChunkName: "view_orders" */ "./views/ViewOrders.vue")
    },
    {
      path: "/order/:id",
      name: "OrderDetail",
      component: () =>
        import(/* webpackChunkName: "order_detail" */ "./views/OrderDetail.vue")
    },
    {
      path: "/stats",
      name: "Stats",
      component: () =>
        import(/* webpackChunkName: "stats" */ "./views/Stats.vue")
    }
  ]
});
