import "@babel/polyfill";
import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import VueGoogleCharts from "vue-google-charts";
import VueGeolocation from "vue-browser-geolocation";

axios.defaults.baseURL = "http://localhost:3000";

Vue.use(VueGeolocation);
Vue.use(VueGoogleCharts);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
